#!/bin/sh
cd "`dirname "$0"`"
if [ -f acoustic_levitation_tweezers.ps ]; then
gs -q -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sPAPERSIZE=a4 -dPDFSETTINGS=/prepress -dAutoRotatePages=/All -sOutputFile=acoustic_levitation_tweezers_sch.pdf acoustic_levitation_tweezers.ps -c "<</Title (Acoustic levitation) /Author (L�a Strobino) /DOCINFO pdfmark"
fi
if [ -f acoustic_levitation_tweezers-F.Cu.ps ]; then
gs -q -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -dAutoRotatePages=/All -sOutputFile=acoustic_levitation_tweezers_pcb.pdf acoustic_levitation_tweezers-B.Cu.ps acoustic_levitation_tweezers-F.Cu.ps acoustic_levitation_tweezers-B.SilkS.ps acoustic_levitation_tweezers-F.SilkS.ps acoustic_levitation_tweezers-B.Mask.ps acoustic_levitation_tweezers-F.Mask.ps -c "<</CropBox [223 343 387 499] /PAGES pdfmark <</Title (Acoustic levitation) /Author (L�a Strobino) /DOCINFO pdfmark"
fi
rm acoustic_levitation_tweezers*.ps
